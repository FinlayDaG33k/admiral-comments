<?php
use Migrations\AbstractMigration;

class AddCommentsTable extends AbstractMigration {
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change() {
    // Create the articles table
    $table = $this->table('comments')
                  ->addColumn('user_id','integer',['default' => null,'limit' => 11,'null' => true])
                  ->addColumn('reply', 'integer', ['default' => null, 'limit' => 11, 'null' => true])
                  ->addColumn('body','text',['default' => null,'null' => false])
                  ->addColumn('posted','datetime',['default' => null,'null' => false])
                  ->addColumn('modified','datetime',['default' => null,'null' => false])
                  ->addForeignKey('user_id','users','id', ['delete'=> 'SET_NULL', 'update'=> 'NO_ACTION'])
                  ->addForeignKey('reply','comments','id', ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
                  ->save();

    // Create the articles_tags table
    $table = $this->table('articles_comments',['id' => false,'primary_key' => ['article_id','comment_id']])
                  ->addColumn('article_id','integer',['default' => null,'limit' => 11,'null' => false])
                  ->addColumn('comment_id','integer',['default' => null,'limit' => 11,'null' => false])
                  ->addForeignKey('article_id','articles','id', ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
                  ->addForeignKey('comment_id','comments','id', ['delete'=> 'CASCADE', 'update'=> 'NO_ACTION'])
                  ->save();
  }
}
