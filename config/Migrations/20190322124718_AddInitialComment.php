<?php
use Migrations\AbstractMigration;
use Cake\ORM\TableRegistry;

class AddInitialComment extends AbstractMigration {
  /**
   * Change Method.
   *
   * More information on this method is available here:
   * http://docs.phinx.org/en/latest/migrations.html#the-change-method
   * @return void
   */
  public function change() {
    $commentsTable = TableRegistry::get('FinlayDaG33k/Comments.Comments');

    $data  = [
      'user_id' => 1,
      'body' => 'Example comment',
      'posted' => date("Y-m-d H:i:s"),
      'modified' => date("Y-m-d H:i:s"),
      'reply' => null,
      'articles' => [
        ['id' => 1]
      ]
    ];

    $entity = $commentsTable->newEntity($data);
    $commentsTable->save($entity);
  }
}
