<?php
  use Cake\Routing\Route\DashedRoute;
  use Cake\Routing\Router;
  use Cake\Routing\RouteBuilder;

  /**
   * Routes for the admin dashboard
   */
  Router::plugin('FinlayDaG33k/Comments',['path' => '/admin'],function ($routes) {
    $routes->get('/comments', ['controller' => 'Comments', 'action' => 'index']);
    $routes->connect('/comments/view/:id', ['controller' => 'Comments', 'action' => 'view'])
      ->setPass(['id']);

    $routes->fallbacks(DashedRoute::class);
  });

  /**
   * Routes for the API
   */
  Router::plugin('FinlayDaG33k/Comments',['path' => '/api/comments'],function ($routes) {
    $routes->get('/getByArticle/:article/:page', ['controller' => 'Api', 'action' => 'getByArticle'])
      ->setPass(['article','page']);
    $routes->get('/getById/:id', ['controller' => 'Api', 'action' => 'getById'])
      ->setPass(['id']);
    $routes->post('/:article', ['controller' => 'Api', 'action' => 'add'])
      ->setPass(['article']);
    $routes->delete('/:id', ['controller' => 'Api', 'action' => 'delete'])
      ->setPass(['id']);

    $routes->fallbacks(DashedRoute::class);
  });