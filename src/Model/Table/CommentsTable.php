<?php
namespace FinlayDaG33k\Comments\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Comments Model
 *
 * @property \FinlayDaG33k\Comments\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 * @property \FinlayDaG33k\Comments\Model\Table\ArticlesTable|\Cake\ORM\Association\BelongsToMany $Articles
 *
 * @method \FinlayDaG33k\Comments\Model\Entity\Comment get($primaryKey, $options = [])
 * @method \FinlayDaG33k\Comments\Model\Entity\Comment newEntity($data = null, array $options = [])
 * @method \FinlayDaG33k\Comments\Model\Entity\Comment[] newEntities(array $data, array $options = [])
 * @method \FinlayDaG33k\Comments\Model\Entity\Comment|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \FinlayDaG33k\Comments\Model\Entity\Comment|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \FinlayDaG33k\Comments\Model\Entity\Comment patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \FinlayDaG33k\Comments\Model\Entity\Comment[] patchEntities($entities, array $data, array $options = [])
 * @method \FinlayDaG33k\Comments\Model\Entity\Comment findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CommentsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('comments');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'className' => 'Kikioboeru/Kikioboeru.Users'
        ]);
        $this->belongsToMany('Articles', [
            'foreignKey' => 'comment_id',
            'targetForeignKey' => 'article_id',
            'joinTable' => 'articles_comments',
            'className' => 'Kikioboeru/Blog.Articles'
        ]);

        $this->belongsTo('Reply', [
            'foreignKey' => 'reply',
            'className' => 'FinlayDaG33k/Comments.Comments',
            'propertyName' => 'reply'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', 'create');

        $validator
            ->scalar('body')
            ->requirePresence('body', 'create')
            ->allowEmptyString('body', false);

        $validator
            ->dateTime('posted')
            ->requirePresence('posted', 'create')
            ->allowEmptyDateTime('posted', false);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
