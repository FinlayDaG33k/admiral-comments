<?php
  namespace FinlayDaG33k\Comments\Form;

  use Cake\Form\Form;
  use Cake\Form\Schema;
  use Cake\Validation\Validator;

  class CommentForm extends Form {
    protected function _buildSchema(Schema $schema) {
      return $schema
        ->addField('user_id', ['type' => 'integer'])
        ->addField('reply', ['type' => 'integer'])
        ->addField('body', ['type' => 'text'])
        ->addField('posted', ['type' => 'datetime'])
        ->addField('modified', ['type' => 'datetime'])
        ->addField('status', ['type' => 'integer']);
    }

    protected function _buildValidator(Validator $validator) {
      return $validator
        ->requirePresence('body')
        ->notEmpty('body', 'Please type in a comment')
        ->add('body', [
          'length' => [
            'rule' => ['minLength', 20],
            'message' => 'Comments need to be atleast 20 characters long'
          ]
        ]);
    }
  }
