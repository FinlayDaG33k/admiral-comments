<?= $this->Form->create(); ?>
  <div class="row">
    <!-- Main "content" -->
    <div class="col-9">
      <div class="row">
        <div class="col-12">
          <div class="card rounded-0">
            <div class="card-header">
              Author
            </div>
            <div class="card-body">
              <div class="form-group row">
                <label for="authorName" class="col-sm-2 col-form-label">Name</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control rounded-0" id="authorName" value="<?php if($comment->user): ?><?= h($comment->user['username']); ?><?php else: ?>Anonymous<?php endif; ?>">
                </div>
              </div>
              <div class="form-group row">
                <label for="authorEmail" class="col-sm-2 col-form-label">Email</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control rounded-0" id="authorEmail" <?php if($comment->user): ?>value="<?= h($comment->user['email']); ?>"<?php endif; ?>>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row mt-2">
        <div class="col-12">
          <div class="form-group">
            <?= $this->Ui->texteditor->create($comment->body, [
              'name' => 'body'
            ]); ?>
          </div>
        </div>
      </div>
    </div>
    <!-- Sidebar -->
    <div class="col-lg-3">
      <div class="row">
        <div class="col-lg-12">
          <div class="card rounded-0">
            <div class="card-header">
              Status
            </div>
            <div class="card-body">
              <div class="form-group row">
                <div class="col-12">
                  <div class="form-check">
          <input class="form-check-input" type="radio" name="published" value="1" <?php if($comment->status == 1): ?>checked<?php endif; ?>>
                    <label class="form-check-label" for="published">
                      Approved
                    </label>
                  </div>
                  <div class="form-check">
                    <input class="form-check-input" type="radio" name="published" value="0" <?php if($comment->status == 0): ?>checked<?php endif; ?>>
                    <label class="form-check-label" for="published">
                      Pending Review
                    </label>
                  </div>
                  <div class="form-check">
                    <input class="form-check-input" type="radio" name="published" value="2" <?php if($comment->status == 2): ?>checked<?php endif; ?>>
                    <label class="form-check-label" for="published">
                      Spam
                    </label>
                  </div>
                </div>
              </div>
              <div class="form-group row">
                <div class="col-12">
                  Submitted on: <?= h($comment->posted); ?>
                  <?php if($comment->posted != $comment->modified): ?>
                    <br />
                    <small class="text-muted">modified on <?= h($comment->posted); ?> </small>
                  <?php endif; ?>
                  <br />
                  In response to:
                  <?= $this->Html->link(
                    $comment->articles[0]->title,
                    ['plugin'=>'Admiral/Blog', 'controller' => 'Blog', 'action' => 'view', 'slug' => $comment->articles[0]->slug],
                    ['target' => '_blank']
                  ); ?>
                  <?php if($comment->reply): ?>
                    <br />
                    In reply to:
                    <?php if($comment->reply->user): ?>
                      <?= $this->Html->link(
                        $comment->reply['user']['username'],
                        ['plugin' => 'FinlayDaG33k/Comments', 'controller' => 'Comments', 'action' => 'view', 'id' => $comment->reply['id']],
                        ['target' => '_blank']
                      ); ?>
                    <?php else: ?>
                      Anonymous
                    <?php endif; ?>
                  <?php endif; ?>
                </div>
              </div>
            </div>
            <div class="card-footer text-muted">
              <?php if($user->hasRight('delete_comments')): ?>
                <?= $this->Form->button('Delete',["class"=>"btn btn-danger rounded-0 float-left"]); ?>
              <?php endif; ?>
              <?php if($user->hasRight('edit_comments')): ?>
                <?= $this->Form->button('Save',["type"=>"submit","class"=>"btn btn-primary rounded-0 float-right"]); ?>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
<?= $this->Form->end(); ?>