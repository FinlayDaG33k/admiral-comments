<?php
  namespace FinlayDaG33k\Comments\Controller;

  use FinlayDaG33k\Comments\Controller\AppController;
  use Cake\Routing\Router;
  use Admiral\Admiral\Permission;
  use Admiral\Admiral\User;

  class CommentsController extends AppController {
    private $redirects;

    public function initialize(){
      parent::initialize();

      $this->loadModel('FinlayDaG33k/Comments.Comments');

      if(in_array($this->request->action, ['index','view'])) {
        $this->viewBuilder()->setLayout('Admiral/Admiral.admin');
      }

      // Define our redirects
      $this->redirects = [
        -1 => [
          'plugin'=>'Admiral/Admiral',
          'controller' => 'Users',
          'action'=>'login',
          'redir' => Router::url([
            'controller' => $this->request->params['controller'], 
            'action' => $this->request->params['action']
          ])
        ],
        0 => [
          'plugin'=>'Admiral/Admiral',
          'controller' => 'Users',
          'action'=>'login'
        ],
        1 => [
          'plugin'=>'Admiral/Admiral',
          'controller' => 'Admin',
          'action'=>'index'
        ]
      ];
    }

    public function index() {
      if(($res = Permission::check('finlaydag33k.comments.view')) != 1) {
        $this->redirect($this->redirects[$res]);
      }

      $comments = $this->Comments->find()->contain(['Articles','Users','Reply' => ['Users']])->order(['Comments.posted'=> 'DESC'])->all();
      $this->set('comments', $comments);
      $this->set('title', 'Comments');
    }

    public function view($id = null) {
      if(($res = Permission::check('finlaydag33k.comments.view')) != 1) {
        $this->redirect($this->redirects[$res]);
      }

      $comment = $this->Comments->findById($id)->contain(['Articles','Users','Reply' => ['Users']])->firstOrFail();

      if($this->request->is('post')) {
        if(($res = Permission::check('finlaydag33k.comments.edit')) != 1) {
          $this->redirect($this->redirects[$res]);
        }

        if($this->request->getData('body') != $comment->body)
          $comment->body = $this->request->getData('body');
        if($this->request->getData('published') != $comment->status)
          $comment->status = $this->request->getData('published');
        if($comment->isDirty())
          $this->Comments->save($comment);
      }

      $this->set('user', User::get());
      $this->set(compact('comment'));
    }
  }